<?php
    require_once("../classes/paymenttype.php");
    require_once("dbutil.php");

    class DBPayment {

        private $dbutil;

        public function __construct() {
            $this->dbutil = new DBUtil();
        }

        public function get_all_payment_types() {
            $sql = "select * from PaymentType;";

            $statement = $this->dbutil->get_connection()->prepare($sql);

            $statement->execute();

            $result = [];
            foreach ($statement->fetchAll() as $row) {
                $result[] = $this->get_object_from_row($row);
            }

            return $result;
        }

        private function get_object_from_row($row) {
            if (empty($row)) {
                return null;
            }

            return new PaymentType($row["paymentTypeId"], $row["description"]);
        }

    }
?>