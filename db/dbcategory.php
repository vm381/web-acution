<?php
    require_once("dbutil.php");
    require_once("../classes/category.php");

    class DBCategory {

        private $dbutil;

        public function __construct() {
            $this->dbutil = new DBUtil();
        }

        public function get_all_categories() {
            $sql = "select * from Category";

            $statement = $this->dbutil->get_connection()->prepare($sql);

            $statement->execute();

            $categories = [];
            foreach ($statement->fetchAll() as $row) {
                $categories[] = $this->get_object_from_row($row);
            }

            return $categories;
        }

        private function get_object_from_row($row) {
            if (empty($row)) {
                return null;
            }

            return new Category($row["categoryId"], $row["name"], $row["description"]);
        }

    }
?>