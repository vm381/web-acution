<?php
    require_once("../classes/deliverytype.php");
    require_once("dbutil.php");

    class DBDelivery {

        private $dbutil;

        public function __construct() {
            $this->dbutil = new DBUtil();
        }

        public function get_all_delivery_types() {
            $sql = "select * from DeliveryType;";

            $statement = $this->dbutil->get_connection()->prepare($sql);

            $statement->execute();

            $result = [];
            foreach ($statement->fetchAll() as $row) {
                $result[] = $this->get_object_from_row($row);
            }

            return $result;
        }

        public function get_type_by_id($id) {
            $sql = "select * from DeliveryType where deliveryTypeId = :id";

            $statement = $this->dbutil->get_connection()->prepare($sql);
            $statement->bindValue("id", $id, PDO::PARAM_STR);

            $statement->execute();

            return $this->get_object_from_row($statement->fetchAll()[0]);
        }

        private function get_object_from_row($row) {
            if (empty($row)) {
                return null;
            }

            return new DeliveryType($row["deliveryTypeId"], $row["description"]);
        }

    }
?>