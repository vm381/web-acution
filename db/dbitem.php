<?php
    require_once("dbutil.php");
    require_once("dboffer.php");
    require_once("../classes/item.php");

    class DBItem {

        private $dbutil;
        private $dboffer;

        public function __construct() {
            $this->dbutil = new DBUtil();
            $this->dboffer = new DBOffer();
        }

        public function search_items($search_term, $search_by) {
            if ($search_term === "") {
                return;
            }

            $sql = "select * from Item where ";
            $result = null;

            if ($search_by === "title") {
                $sql = $sql . "name like :search_term and dateAdded < now() and dateExpired > now();";

                $statement = $this->dbutil->get_connection()->prepare($sql);
                $statement->bindValue("search_term", "%" . $search_term . "%", PDO::PARAM_STR);
                $statement->execute();

                $result = $statement->fetchAll();
            }
            else if ($search_by === "description") {
                $sql = $sql . "description like :search_term and dateAdded < now() and dateExpired > now();";

                $statement = $this->dbutil->get_connection()->prepare($sql);
                $statement->bindValue("search_term", "%" . $search_term . "%", PDO::PARAM_STR);
                $statement->execute();

                $result = $statement->fetchAll();
            }
            else if ($search_by === "category") {
                $statement = $this->dbutil->get_connection()->prepare("select categoryId from Category where name like :search_term;");
                $statement->bindValue("search_term", "%" . $search_term . "%", PDO::PARAM_STR);
                $statement->execute();
                $category = $statement->fetch();

                $statement = $this->dbutil->get_connection()->prepare("select * from Item where category = :category and dateAdded < now() and dateExpired > now()");
                $statement->bindValue("category", $category["categoryId"], PDO::PARAM_STR);

                $statement->execute();
                $result = $statement->fetchAll();
            }
            
            return $this->create_objects($result);
        }

        private function create_objects($db_rows) {
            $result = [];
            foreach ($db_rows as $row) {
                $item = $this->get_object_from_row($row);

                $result[] = $item;
            }

            return $result;
        }

        public function get_item_by_id($itemId) {
            $sql = "select * from Item where itemId = :itemId";

            $statement = $this->dbutil->get_connection()->prepare($sql);
            $statement->bindValue("itemId", $itemId, PDO::PARAM_INT);

            $statement->execute();
            $result = $statement->fetchAll()[0];

            return $this->get_object_from_row($result);
        }

        public function get_users_items($user) {
            $sql = "select * from Item where seller = :user";

            $statement = $this->dbutil->get_connection()->prepare($sql);
            $statement->bindValue("user", $user, PDO::PARAM_STR);

            $statement->execute();
            
            $result = [];
            foreach ($statement->fetchAll() as $row) {
                $result[] = $this->get_object_from_row($row);
            }

            return $result;
        }

        private function get_object_from_row($row) {
            if (empty($row)) {
                return null;
            }

            $item = new Item($row["itemId"], $row["name"], $row["description"], $row["dateAdded"], $row["dateExpired"], $row["startPrice"], $row["seller"], $row["photo"], $row["category"], $row["deliveryType"], $row["paymentType"], $row["canceled"]);
            $bestOffer = $this->dboffer->get_best_offer($row["itemId"]);
            if ($bestOffer) {
                $item->setBestOffer($bestOffer);
            }

            return $item;
        }

        public function insert_item($item) {
            $sql = "insert into Item (name, description, dateAdded, dateExpired, startPrice, photo, seller, deliveryType, paymentType, category)
                values (:name, :description, now(), date_add(now(), interval 10 day), :price, :photo, :seller, :delivery, :payment, :category);";
            
            $statement = $this->dbutil->get_connection()->prepare($sql);
            $statement->bindValue("name", $item->getName(), PDO::PARAM_STR);
            $statement->bindValue("description", $item->getDescription(), PDO::PARAM_STR);
            $statement->bindValue("price", $item->getStartPrice(), PDO::PARAM_INT);
            $statement->bindValue("photo", $item->getPhoto(), PDO::PARAM_STR);
            $statement->bindValue("seller", $item->getSeller(), PDO::PARAM_STR);
            $statement->bindValue("delivery", $item->getDeliveryType(), PDO::PARAM_INT);
            $statement->bindValue("payment", $item->getPaymentType(), PDO::PARAM_INT);
            $statement->bindValue("category", $item->getCategory(), PDO::PARAM_INT);

            return $statement->execute();
        }

        public function cancel_auction($itemId) {
            $sql = "update Item set canceled = true where itemId = :id";

            $statement = $this->dbutil->get_connection()->prepare($sql);
            $statement->bindValue("id", $itemId, PDO::PARAM_INT);

            return $statement->execute();
        }

        public function get_finished_auctions($user) {
            $sql = "select * from Item where seller = :user and dateExpired < now() and not canceled order by dateExpired desc;";

            $statement = $this->dbutil->get_connection()->prepare($sql);
            $statement->bindValue("user", $user, PDO::PARAM_STR);

            $statement->execute();

            $result = [];
            foreach ($statement->fetchAll() as $row) {
                $result[] = $this->get_object_from_row($row);
            }

            return $result;
        }

        public function get_won_auctions($user) {
            $sql = "select * from Item where dateExpired < now() and not canceled;";
            
            $statement = $this->dbutil->get_connection()->prepare($sql);
            $statement->execute();

            $result = [];
            foreach ($statement->fetchAll() as $row) {
                $item = $this->get_object_from_row($row);
                if ($item->getBestOffer()->getUser() === $user) {
                    $result[] = $item;
                }
            }

            return $result;
        }

    }
?>