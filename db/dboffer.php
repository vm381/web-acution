<?php
    require_once("dbutil.php");
    require_once("../classes/offer.php");

    class DBOffer {

        private $dbutil;

        public function __construct() {
            $this->dbutil = new DBUtil();
        }

        public function get_best_offer($itemId) {
            $sql = "select * from Offer where item = :item and amount = (select max(amount) from Offer where item = :item and not canceled) and not canceled;";

            $statement = $this->dbutil->get_connection()->prepare($sql);
            $statement->bindValue("item", $itemId, PDO::PARAM_INT);
            try {
                $statement->execute();
                $result = $statement->fetchAll();
                if (!empty($result)) {
                    return $this->get_object($result[0]);
                }
                else {
                    return null;
                }
            }
            catch (PDOException $e) {
                return null;
            }
        }

        public function cancel_offer($user, $item) {
            $sql = "update Offer set canceled = true where user = :user and item = :item";

            $statement = $this->dbutil->get_connection()->prepare($sql);
            $statement->bindValue("user", $user, PDO::PARAM_STR);
            $statement->bindValue("item", $item, PDO::PARAM_INT);

            return $statement->execute();
        }

        public function insert_offer($offer) {
            $canceled = "select * from Offer where user = :user and item = :item and canceled = true;";
            $stmt = $this->dbutil->get_connection()->prepare($canceled);
            $stmt->bindValue("user", $offer->getUser(), PDO::PARAM_STR);
            $stmt->bindValue("item", $offer->getItemId(), PDO::PARAM_INT);

            $stmt->execute();
            $canceledOffers = $stmt->fetchAll();

            if (!empty($canceledOffers)) {
                return -1;
            }

            $sql = "call insert_offer(:amount, :user, :item, @succ);";

            $statement = $this->dbutil->get_connection()->prepare($sql);
            $statement->bindValue("amount", $offer->getAmount(), PDO::PARAM_STR);
            $statement->bindValue("user", $offer->getUser(), PDO::PARAM_STR);
            $statement->bindValue("item", $offer->getItemId(), PDO::PARAM_STR);

            try {
                $statement->execute();
                $result = $statement->fetchAll()[0]["success"];
                if ($result) {
                    return 1;
                }
                else {
                    return 0;
                }
            }
            catch (PDOException $e) {
                return 0;
            }
        }

        public function get_offers_for_item($itemId) {
            $sql = "select * from Offer where item = :itemId and not canceled order by time desc";

            $statement = $this->dbutil->get_connection()->prepare($sql);
            $statement->bindValue("itemId", $itemId, PDO::PARAM_INT);

            $statement->execute();
            
            $result = [];
            foreach ($statement->fetchAll() as $row) {
                $result[] = $this->get_object($row);
            }

            return $result;
        }

        private function get_object($row) {
            if (empty($row)) {
                return null;
            }

            return new Offer($row["offerId"], $row["amount"], $row["time"], $row["canceled"], $row["user"], $row["item"]);
        }

    }
?>