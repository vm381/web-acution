<?php
    function validateUser($email, $password, $firstName, $lastName, &$error) {
        if ($email === false) {
            $error[] = "Email must be valid.";
        }
        if (strlen($password) < 5) {
            $error[] = "Password must be at least 5 characters long.";
        }
        if (empty($firstName)) {
            $error[] = "First name can't be empty.";
        }
        if (empty($lastName)) {
            $error[] = "Last name can't be empty.";
        }
    }

    function validateNewItem($name, $price, &$errors) {
        if (trim($name) === "") {
            $errors[] = "Item name is required.";
        }
        if (filter_var($price, FILTER_VALIDATE_INT) === false) {
            $errors[] = "Price must be a number.";
        }
        else if ($price < 1) {
            $errors[] = "Price must be a positive number.";
        }
    }
?>