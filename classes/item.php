<?php
    require_once("../helper/checkLogged.php");
    require_once("offer.php");

    class Item {

        private $itemId;
        private $name;
        private $description;
        private $dateAdded;
        private $dateExpired;
        private $startPrice;
        private $seller;
        private $photo;
        private $category;
        private $deliveryType;
        private $paymentType;
        private $canceled;

        private $bestOffer;

        public function __construct($itemId, 
            $name, 
            $description, 
            $dateAdded, 
            $dateExpired, 
            $startPrice, 
            $seller, 
            $photo, 
            $category, 
            $deliveryType, 
            $paymentType, 
            $canceled) {
                $this->itemId = $itemId;
                $this->name = $name;
                $this->description = $description;
                $this->dateAdded = $dateAdded;
                $this->dateExpired = $dateExpired;
                $this->startPrice = $startPrice;
                $this->seller = $seller;
                $this->photo = $photo;
                $this->category = $category;
                $this->deliveryType = $deliveryType;
                $this->paymentType = $paymentType;
                $this->canceled = $canceled;
        }

        public function getItemId() {
            return $this->itemId;
        }

        public function getName() {
            return $this->name;
        }

        public function getDescription() {
            return $this->description;
        }

        public function getDateAdded() {
            return $this->dateAdded;
        }

        public function getDateExpired() {
            return $this->dateExpired;
        }

        public function getStartPrice() {
            return $this->startPrice;
        }

        public function getSeller() {
            return $this->seller;
        }

        public function getPhoto() {
            return $this->photo;
        }

        public function getCategory() {
            return $this->category;
        }

        public function getDeliveryType() {
            return $this->deliveryType;
        }

        public function getPaymentType() {
            return $this->paymentType;
        }

        public function getCanceled() {
            return $this->canceled;
        }

        public function getBestOffer() {
            return $this->bestOffer;
        }

        public function setBestOffer($bestOffer) {
            $this->bestOffer = $bestOffer;
        }

        public function setCanceled($canceled) {
            $this->canceled = $canceled;
        }

        public function to_html() {
            $html = "<div style=\"border: solid 1px black; margin-bottom: 65px\">";
            $html .= "<span style=\"text-align: center; font-weight: bold; font-size: 18px;\">{$this->name}</span><br>";
            $html .= "<span style=\"text-align: center;\">{$this->description}</span><br>";
            $html .= "<span style=\"text-align: center;\">Auction start: {$this->dateAdded}</span><br>";
            $html .= "<span style=\"text-align: center;\">Auction end: {$this->dateExpired}</span><br>";
            $html .= "<span style=\"text-align: center;\">Start price: \${$this->startPrice}</span><br>";
            if ($this->getPhoto() != "") {
                $html .= "<span style=\"text-align: center;\"><img width=200px height=200px src=\"{$this->photo}\"></span><br>";
            }
            if ($this->bestOffer) {
                $html .= "<span style=\"text-align: center;\">Best offer: \${$this->bestOffer->getAmount()} ({$this->bestOffer->getUser()})";
                if (checkLogged() && $_SESSION["user"]->getEmail() === $this->bestOffer->getUser()) {
                    $html .= " <a href=\"cancelOffer.php?itemId={$this->itemId}&user={$this->bestOffer->getUser()}\" ><button>Cancel offer</button></a>";
                }
                $html .= "</span><br>";
                if (checkLogged() && $_SESSION["user"]->getEmail() !== $this->getSeller() && $_SESSION["user"]->getEmail() !== $this->bestOffer->getUser()) {
                    $html .= "<span style=\"text-align: center;\">
                        <form action=\"search.php\" method=\"post\">
                        <input type=\"number\" name=\"offer\">
                        <input type=\"submit\" name=\"bid\" value=\"Bid\">
                        <input type=\"hidden\" name=\"itemId\" value=\"{$this->itemId}\">
                        </form></span>";
                }
                else if (checkLogged() && $_SESSION["user"]->getEmail() === $this->getSeller()) {
                    $html .= "<span><a href=\"auctionDetails.php?itemId={$this->itemId}\"><button>Details</button></a></span><br>";
                }
            }
            else {
                $html .= "<span style=\"text-align: center;\">No offers</span><br>";
                if (checkLogged() && $_SESSION["user"]->getEmail() !== $this->getSeller()) {
                    $html .= "<span style=\"text-align: center;\">
                        <form action=\"search.php\" method=\"post\">
                        <input type=\"number\" name=\"offer\">
                        <input type=\"submit\" name=\"bid\" value=\"Bid\">
                        <input type=\"hidden\" name=\"itemId\" value=\"{$this->itemId}\">
                        </form></span>";
                }
                else if (checkLogged() && $_SESSION["user"]->getEmail() === $this->getSeller()) {
                    $html .= "<span><a href=\"auctionDetails.php?itemId={$this->itemId}\"><button>Details</button></a></span><br>";
                }
            }
            if (isset($_SESSION["user"]) && $_SESSION["user"]->getEmail() !== $this->getSeller()) {
                $html .= "<span style=\"text-align: center;\">By {$this->seller}</span><br>";
            }
            $html .= "</div>";

            return $html;
        }

    }
?>