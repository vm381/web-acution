<?php
    class User {

        private $email;
        private $firstName;
        private $lastName;
        private $password;

        public function __construct($email, $firstName, $lastName, $password = "") {
            $this->email = $email;
            $this->firstName = $firstName;
            $this->lastName = $lastName;
            $this->password = $password;
        }

        public function getEmail() {
            return $this->email;
        }

        public function getPassword() {
            return $this->password;
        }

        public function getFirstName() {
            return $this->firstName;
        }

        public function getLastName() {
            return $this->lastName;
        }

    }
?>