<?php
    class DeliveryType {

        private $deliveryTypeId;
        private $description;

        public function __construct($deliveryTypeId, $description) {
            $this->deliveryTypeId = $deliveryTypeId;
            $this->description = $description;
        }

        public function getDeliveryTypeId() {
            return $this->deliveryTypeId;
        }

        public function getDescription() {
            return $this->description;
        }

    }
?>