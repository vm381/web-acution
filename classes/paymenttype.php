<?php
    class PaymentType {

        private $paymentTypeId;
        private $description;

        public function __construct($paymentTypeId, $description) {
            $this->paymentTypeId = $paymentTypeId;
            $this->description = $description;
        }

        public function getPaymentTypeId() {
            return $this->paymentTypeId;
        }

        public function getDescription() {
            return $this->description;
        }

    }
?>