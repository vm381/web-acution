<?php
    class Category {

        private $categoryId;
        private $categoryName;
        private $description;

        public function __construct($categoryId, $categoryName, $description) {
            $this->categoryId = $categoryId;
            $this->categoryName = $categoryName;
            $this->description = $description;
        }

        public function getCategoryId() {
            return $this->categoryId;
        }

        public function getCategoryName() {
            return $this->categoryName;
        }

        public function getDescription() {
            return $this->description;
        }

    }
?>