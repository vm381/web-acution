<?php
    class Offer {

        private $offerId;
        private $amount;
        private $time;
        private $canceled;
        private $user;
        private $itemId;

        public function __construct($offerId, $amount, $time, $canceled, $user, $itemId) {
            $this->offerId = $offerId;
            $this->amount = $amount;
            $this->time = $time;
            $this->canceled = $canceled;
            $this->user = $user;
            $this->itemId = $itemId;
        }

        public function getOfferId() {
            return $this->offerId;
        }

        public function getAmount() {
            return $this->amount;
        }

        public function getTime() {
            return $this->time;
        }

        public function getCanceled() {
            return $this->canceled;
        }

        public function getUser() {
            return $this->user;
        }

        public function getItemId() {
            return $this->itemId;
        }

    }
?>