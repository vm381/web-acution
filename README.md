# Web Acution

Test project for PHP internship program at Librafire.

Projekat je radjen pod LAMP okruzenjem.

Verovatno ima bagova koje nisam primetio, jer je testiranje radjeno manuelno. 

Fokus je bio na backend-u, tako da dizajn nije bas neki.

Da bi upload fajlova radio potrebno je da apache ima read-write permisije nad /images folderom.

Dva .sql fajla, jedan je samo sema baze, bez ikakvih podataka, drugi je export baze sa test podacima. Config fajl za povezivanje PHP-a sa MySQL bazom se nalazi u folderu /config.
