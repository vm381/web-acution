CREATE TABLE `User` (
	`email` varchar(45) NOT NULL,
	`firstName` varchar(45) NOT NULL,
	`lastName` varchar(45) NOT NULL,
	`password` varchar(245) NOT NULL,
	PRIMARY KEY (`email`)
);

CREATE TABLE `Item` (
	`itemId` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(45) NOT NULL,
	`description` varchar(245) NOT NULL,
	`dateAdded` DATETIME NOT NULL,
	`dateExpired` DATETIME NOT NULL,
	`startPrice` DECIMAL NOT NULL,
	`photo` varchar(255),
	`seller` varchar(45) NOT NULL,
	`deliveryType` INT NOT NULL,
	`paymentType` INT NOT NULL,
	`category` INT NOT NULL,
	`canceled` BOOLEAN NOT NULL DEFAULT false,
	PRIMARY KEY (`itemId`)
);

CREATE TABLE `DeliveryType` (
	`deliveryTypeId` INT NOT NULL AUTO_INCREMENT,
	`description` varchar(45) NOT NULL UNIQUE,
	PRIMARY KEY (`deliveryTypeId`)
);

CREATE TABLE `Offer` (
	`offerId` INT NOT NULL AUTO_INCREMENT,
	`amount` DECIMAL NOT NULL,
	`time` DATETIME NOT NULL,
	`canceled` BOOLEAN NOT NULL DEFAULT false,
	`user` varchar(45) NOT NULL,
	`item` INT NOT NULL,
	PRIMARY KEY (`offerId`)
);

CREATE TABLE `PaymentType` (
	`paymentTypeId` INT NOT NULL AUTO_INCREMENT,
	`description` varchar(45) NOT NULL UNIQUE,
	PRIMARY KEY (`paymentTypeId`)
);

CREATE TABLE `Category` (
	`categoryId` INT NOT NULL AUTO_INCREMENT,
	`name` varchar(45) NOT NULL UNIQUE,
	`description` varchar(245) NOT NULL,
	PRIMARY KEY (`categoryId`)
);

ALTER TABLE `Item` ADD CONSTRAINT `Item_fk0` FOREIGN KEY (`seller`) REFERENCES `User`(`email`);

ALTER TABLE `Item` ADD CONSTRAINT `Item_fk1` FOREIGN KEY (`deliveryType`) REFERENCES `DeliveryType`(`deliveryTypeId`);

ALTER TABLE `Item` ADD CONSTRAINT `Item_fk2` FOREIGN KEY (`paymentType`) REFERENCES `PaymentType`(`paymentTypeId`);

ALTER TABLE `Item` ADD CONSTRAINT `Item_fk3` FOREIGN KEY (`category`) REFERENCES `Category`(`categoryId`);

ALTER TABLE `Offer` ADD CONSTRAINT `Offer_fk0` FOREIGN KEY (`user`) REFERENCES `User`(`email`);

ALTER TABLE `Offer` ADD CONSTRAINT `Offer_fk1` FOREIGN KEY (`item`) REFERENCES `Item`(`itemId`);

delimiter //

create procedure insert_offer(in amount decimal, in user varchar(45), in item_id int, out success boolean)
begin
	declare p_item_added DATETIME;
	declare p_item_expiring DATETIME;
	declare p_datetime_now DATETIME;
	declare p_start_price DECIMAL;
	declare p_best_offer DECIMAL;

	select Item.dateAdded, Item.dateExpired into p_item_added, p_item_expiring from Item where Item.itemId = item_id;
	set p_datetime_now = NOW();
	select Item.startPrice into p_start_price from Item where Item.itemId = item_id;
	select max(Offer.amount) into p_best_offer from Offer where Offer.item = item_id and not Offer.canceled;

	if p_datetime_now > p_item_added and p_datetime_now < p_item_expiring and amount > p_start_price and (p_best_offer is null or amount > p_best_offer)
	then
		insert into Offer (amount, time, user, item) values (amount, p_datetime_now, user, item_id);
		set success = true;
	else
		 set success = false;
	end if;

	select success;
end//

delimiter ;
