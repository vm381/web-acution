<?php
    require_once("../db/dbitem.php");
    require_once("../classes/item.php");
    require_once("../helper/checkLogged.php");
    require_once("../db/dboffer.php");
?>

<?php include("header.php"); ?>

<?php
    if (!checkLogged()) {
        header("Location: /");
    }

    if (isset($_GET["itemId"]) && isset($_GET["user"])) {
        $itemId = htmlspecialchars($_GET["itemId"]);
        $user = htmlspecialchars($_GET["user"]);

        $dbitem = new DBItem();
        $item = $dbitem->get_item_by_id($itemId);

        if (isset($_GET["cancel"])) {
            $dboffer = new DBOffer();
            $dboffer->cancel_offer($user, $itemId);

            header("Location: search.php?search={$_SESSION['search']}&searchBy={$_SESSION['searchBy']}");
        }
    }
    else {
        //header("Location: /");
    }
?>
<center>
    <div class="confirm">
        Are you sure you want to cancel offer for item <?php echo $item->getName(); ?>?<br>
        <a href="cancelOffer.php?cancel=yes&itemId=<?php echo $_GET["itemId"]; ?>&user=<?php echo $_GET["user"] ?>"><button style="margin: 5px">Yes</button></a>
        <a href="search.php?search=<?php echo $_SESSION["search"]; ?>&searchBy=<?php echo $_SESSION["searchBy"]; ?>"><button style="margin: 5px">No</button></a>
    </div>
</center>
<?php include("footer.php"); ?>