<?php require_once("../helper/checkLogged.php"); ?>
<?php require_once("../classes/user.php"); ?>

<?php session_start(); ?>

<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" type="text/css" href="../styles/style.css">
    <script src="../js/script.js"></script>
    <title>Web Auction</title>
</head>
<body>
<header>
<ul>
  <li><a href="../index.php">Search</a></li>
  <?php if (checkLogged()) { ?>
    <li><a href="../template/createAuction.php">Create auction</a></li>
    <li><a href="../template/myAuctions.php">My auctions</a></li>
    <li><a href="../template/finishedAuctions.php">Finished auctions</a></li>
    <li><a href="../template/account.php">Account</a></li>
    <li><a href="../helper/logout.php">Logout</a></li>
    <li style="float: right;">Logged in as: <?php echo $_SESSION["user"]->getFirstName() . " " . $_SESSION["user"]->getLastName() . " (" . $_SESSION["user"]->getEmail() . ")" ?></li>
  <?php } ?>
  <?php if (!checkLogged()) { ?>
    <li><a href="login.php">Login</a></li>
    <li><a href="register.php">Register</a></li>
  <?php } ?>
</ul>
</header>