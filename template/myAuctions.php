<?php
    require_once("../helper/checkLogged.php"); 
    require_once("../db/dbitem.php");
    require_once("../classes/item.php");
    require_once("../classes/user.php");
?>

<?php include("header.php"); ?>

<?php
    if (!checkLogged()) {
        header("Location: /");
    }
?>

<center>
    <h3>My auctions</h3>
    <?php
        $dbitem = new DBItem();
        $items = $dbitem->get_users_items($_SESSION["user"]->getEmail());

        foreach ($items as $item) {
            echo $item->to_html();
        }
    ?>
</center>

<?php include("footer.php"); ?>