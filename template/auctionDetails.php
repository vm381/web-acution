<?php
    require_once("../helper/checkLogged.php"); 
    require_once("../db/dbitem.php");
    require_once("../classes/item.php");
    require_once("../classes/user.php");
    require_once("../db/dbdelivery.php");
    require_once("../classes/deliverytype.php");
    require_once("../db/dboffer.php");
    require_once("../classes/offer.php");
?>

<?php include("header.php"); ?>

<?php
    if (!checkLogged()) {
        header("Location: /");
    }

    $dbitem = new DBItem();
    $item = $dbitem->get_item_by_id(htmlspecialchars($_GET["itemId"]));

    if (isset($_GET["action"]) && $_GET["action"] === "cancel") {
        if ($dbitem->cancel_auction($item->getItemId())) {
            $item->setCanceled(true);
        }
    }
?>

<center>
    <h3>Details for <?php echo $item->getName(); ?></h3>
    <?php
        if ($item->getDateExpired() < date("Y-m-d H:i:s") && !$item->getCanceled()) {
            ?>
                <div style="color: green;">Auction expired. Best offer is $<?php echo $item->getBestOffer()->getAmount(); ?> by <?php echo $item->getBestOffer()->getUser(); ?>.</div><br>
            <?php
        }
    ?>
    <table border="1">
        <tr>
            <td>Name: </td>
            <td><?php echo $item->getName(); ?></td>
        </tr>
        <tr>
            <td>Description: </td>
            <td><?php echo $item->getDescription(); ?></td>
        </tr>
        <tr>
            <td>Date added: </td>
            <td><?php echo $item->getDateAdded(); ?></td>
        </tr>
        <tr>
            <td>Date expiring: </td>
            <td><?php echo $item->getDateExpired(); ?></td>
        </tr>
        <tr>
            <td>Start price: </td>
            <td><?php echo $item->getStartPrice(); ?></td>
        </tr>
        <tr>
            <td>Photo: </td>
            <td>
                <?php if ($item->getPhoto() !== "") { ?>
                <img src="<?php echo $item->getPhoto(); ?>" width=200px height=200px>
                <?php } ?>
            </td>
        </tr>
        <tr>
            <td>Delivery type: </td>
            <td><?php
                $dbdelivery = new DBDelivery();
                $deliverytype = $dbdelivery->get_type_by_id($item->getDeliveryType());
                echo $deliverytype->getDescription();
            ?></td>
        </tr>
        <tr>
            <td>Cancel: </td>
            <td><?php if (!$item->getCanceled() && $item->getDateExpired() > date("Y-m-d H:i:s")) { ?>
                <a href="auctionDetails.php?action=cancel&itemId=<?php echo $item->getItemId(); ?>"><button>Cancel auction</button></a>
            <?php } else { ?>
                Auction canceled or expried.
            <?php } ?></td>
        </tr>
    </table>
    <br>
    <h4>Offers: </h3>
    <table border="1">
        <tr>
            <th>User</th>
            <th>Time</th>
            <th>Amount</th>
        </tr>
        <?php
            $dboffer = new DBOffer();
            $offers = $dboffer->get_offers_for_item($item->getItemId());
            foreach ($offers as $offer) {
                ?>
                    <tr>
                        <td><?php echo $offer->getUser(); ?></td>
                        <td><?php echo $offer->getTime(); ?></td>
                        <td><?php echo "\$" . $offer->getAmount(); ?></td>
                    </tr>
                <?php
            }
        ?>
    </table>
</center>

<?php include("footer.php"); ?>