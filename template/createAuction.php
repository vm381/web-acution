<?php
    require_once("../classes/item.php");
    require_once("../db/dbitem.php");
    require_once("../classes/user.php");

    require_once("../helper/checkLogged.php");

    require_once("../db/dbcategory.php");
    require_once("../classes/category.php");
    require_once("../db/dbpayment.php");
    require_once("../classes/paymenttype.php");
    require_once("../db/dbdelivery.php");
    require_once("../classes/deliverytype.php");

    require_once("../helper/validateInput.php");
?>

<?php include("header.php"); ?>
<?php
    if (!checkLogged()) {
        header("Location: /");
    }

    $dbcategory = new DBCategory();
    $categories = $dbcategory->get_all_categories();

    $dbpayment = new DBPayment();
    $paymenttypes = $dbpayment->get_all_payment_types();

    $dbdelivery = new DBDelivery();
    $deliverytypes = $dbdelivery->get_all_delivery_types();

    $errors = [];
    $success = "";
    $error = "";

    if (isset($_POST["submit"])) {
        $name = htmlspecialchars($_POST["name"]);
        $description = htmlspecialchars($_POST["description"]);
        $price = htmlspecialchars($_POST["startPrice"]);
        $deliveryType = htmlspecialchars($_POST["delivery"]);
        $paymentType = htmlspecialchars($_POST["payment"]);
        $categoryId = htmlspecialchars($_POST["category"]);

        validateNewItem($name, $price, $errors);

        if (empty($errors)) {
            $filename = "";
            if (isset($_FILES["photo"]) && is_uploaded_file($_FILES["photo"]["tmp_name"])) {
                $filename = "../images/" . basename($_FILES["photo"]["name"]);
                move_uploaded_file($_FILES["photo"]["tmp_name"], $filename);
            }

            $dbitem = new DBItem();
            $item = new Item(null, $name, $description, null, null, $price, $_SESSION["user"]->getEmail(), $filename, (int)$categoryId, (int)$deliveryType, (int)$paymentType, false);
            
            if ($dbitem->insert_item($item)) {
                $success = "Auction successfully created.";
            }
            else {
                $error = "Failed to create auction.";
            }
        }
    }
?>
<center>
    <h3>Create new auction</h3>
    <form method="post" enctype="multipart/form-data" id="createAuction">
        <table>
            <tr>
                <td>Item name: </td>
                <td><input type="text" name="name"></td>
            </tr>
            <tr>
                <td>Item description: </td>
                <td><textarea rows="5" cols="45" name="description"></textarea></td>
            </tr>
            <tr>
                <td>Start price: </td>
                <td><input type="number" name="startPrice"></td>
            </tr>
            <tr>
                <td>Photo: </td>
                <td><input type="file" name="photo"></td>
            </tr>
            <tr>
                <td>Delivery type: </td>
                <td>
                    <select name="delivery">
                        <?php
                            foreach ($deliverytypes as $delivery) {
                                ?>
                                    <option value="<?php echo $delivery->getDeliveryTypeId(); ?>"><?php echo $delivery->getDescription(); ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Payment type: </td>
                <td>
                    <select name="payment">
                        <?php
                            foreach ($paymenttypes as $payment) {
                                ?>
                                    <option value="<?php echo $payment->getPaymentTypeId(); ?>"><?php echo $payment->getDescription(); ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td>Category: </td>
                <td>
                    <select name="category">
                        <?php
                            foreach ($categories as $category) {
                                ?>
                                    <option value="<?php echo $category->getCategoryId(); ?>"><?php echo $category->getCategoryName(); ?></option>
                                <?php
                            }
                        ?>
                    </select>
                </td>
            </tr>
            <tr>
                <td><input type="submit" name="submit" value="Create"></td>
            </tr>
        </table>
    </form>
    
    <?php if (!empty($errors) || !empty($error)) { ?>
        <div class="error"><?php foreach ($errors as $err) echo $err . "<br>"; ?></div>
    <?php } ?>
    <?php if (!empty($error)) { ?>
        <div class="error"><?php echo $error . "<br>"; ?></div>
    <?php } ?>
    <?php if (!empty($success)) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
</center>

<?php include("footer.php"); ?>