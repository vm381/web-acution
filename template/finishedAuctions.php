<?php
    require_once("../helper/checkLogged.php"); 
    require_once("../db/dbitem.php");
    require_once("../classes/item.php");
    require_once("../db/dbuser.php");
?>

<?php include("header.php"); ?>

<?php
    if (!checkLogged()) {
        header("Location: /");
    }

    $dbitem = new DBItem();
    $soldItems = $dbitem->get_finished_auctions($_SESSION["user"]->getEmail());
    $boughtitems = $dbitem->get_won_auctions($_SESSION["user"]->getEmail());

    $dbuser = new DBUser();
?>

<center>
    <h3>Finished auctions</h3>
    <?php
        foreach ($soldItems as $item) {
            $winner = $dbuser->find_by_email($item->getBestOffer()->getUser());
            ?>
                <div>Auction for <a href="auctionDetails.php?itemId=<?php echo $item->getItemId(); ?>"><?php echo $item->getName(); ?></a> expired. Best offer is by <?php echo $winner->getFirstName() . " " . $winner->getLastName(); ?></div>
                <br>
            <?php
        }

        foreach ($boughtitems as $item) {
            $seller = $dbuser->find_by_email($item->getSeller());
            ?>
                <div>You won auction for <a href=""><?php echo $item->getName(); ?></a> from <?php echo $seller->getFirstName() . " " . $seller->getLastName(); ?>.</div>
            <?php
        }
    ?>
</center>

<?php include("footer.php"); ?>