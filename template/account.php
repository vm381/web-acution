<?php
    require_once("../helper/checkLogged.php");
    require_once("../helper/validateInput.php");
    require_once("../classes/user.php");
    require_once("../db/dbuser.php");
?>

<?php
    include("header.php");
?>

<?php
    if (!checkLogged()) {
        header("Location: /");
    }

    $user = $_SESSION["user"];

    $error = [];
    $successful = "";
    $unsuccessful = "";

    if (isset($_POST["update"])) {
        $email = filter_var(trim(htmlspecialchars($_POST["email"])), FILTER_VALIDATE_EMAIL);
        $password = trim(htmlspecialchars($_POST["password"]));
        $firstName = trim(htmlspecialchars($_POST["firstName"]));
        $lastName = trim(htmlspecialchars($_POST["lastName"]));

        validateUser($email, $password, $firstName, $lastName, $error);

        if (empty($error)) {
            $updatedUser = new User($email, $firstName, $lastName, $password);
            $dbuser = new DBUser();
            
            if ($dbuser->update_user($updatedUser)) {
                $successful = "User details successfully updated.";
                $_SESSION["user"] = $updatedUser;
                $user = $updatedUser;
            }
            else {
                $unsuccessful = "User details did not successfully updated. Maybe email is already taken.";
            }
        }
    }
?>

<center>
    <h3>Account details</h3>
    <form method="post" id="accountForm">
        <table>
            <tr>
                <td>Email: </td>
                <td><input type="text" name="email" value="<?php echo $user->getEmail(); ?>"></td>
            </tr>
            <tr>
                <td>Password: </td>
                <td><input type="password" name="password"></td>
            </tr>
            <tr>
                <td>First name: </td>
                <td><input type="text" name="firstName" value="<?php echo $user->getFirstName(); ?>"></td>
            </tr>
            <tr>
                <td>Last name: </td>
                <td><input type="text" name="lastName" value="<?php echo $user->getLastName(); ?>"></td>
            </tr>
            <tr>
                <td><input type="submit" value="Update" name="update"></td>
            </tr>
        </table>
    </form>

    <?php
        if (!empty($error)) {
    ?>
        <div class="error">
            <?php foreach ($error as $err) {
                echo  $err . "<br>";
            } ?>
        </div>
    <?php
        }
    ?>
    <?php
        if (!empty($successful)) {
            ?>
                <div class="success"><?php echo $successful; ?></div>
            <?php
        }
        if (!empty($unsuccessful)) {
            ?>
                <div class="error"><?php echo $unsuccessful; ?></div>
            <?php
        }
    ?>
</center>

<?php
    include("footer.php");
?>