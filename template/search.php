<?php 
    require_once("../db/dbitem.php");
    require_once("../classes/item.php");
    require_once("../db/dboffer.php");
    require_once("../classes/offer.php");
    require_once("../classes/user.php");
?>

<?php include("header.php"); ?>

<?php
    $error = "";
    $success = "";
?>
<center>
    <form method="get" id="searchForm">
        <label>Search: </label>
        <input type="text" name="search">
        <label>Search by: </label>
        <select name="searchBy">
            <option value="title">Title</option>
            <option value="description">Description</option>
            <option value="category">Category</option>
        </select>
        <br>
        <br>
        <input type="submit" value="Search">
    </form>

    <?php
        if (isset($_GET["search"]) && !empty($_GET["search"])) {
            $dbitem = new DBItem();
            $items = $dbitem->search_items(htmlspecialchars($_GET["search"]), htmlspecialchars($_GET["searchBy"]));

            foreach ($items as $item) {
                if (!$item->getCanceled()) {
                    echo $item->to_html();
                }
            }

            $_SESSION["search"] = htmlspecialchars($_GET["search"]);
            $_SESSION["searchBy"] = htmlspecialchars($_GET["searchBy"]);
        }

        if (isset($_POST["bid"])) {
            $bid = filter_var(htmlspecialchars($_POST["offer"]), FILTER_VALIDATE_INT);
            $itemId = htmlspecialchars($_POST["itemId"]);
            if ($bid) {
                $dboffer = new DBOffer();
                $offer = new Offer(0, $bid, "", false, $_SESSION["user"]->getEmail(), $itemId);
                $ok = $dboffer->insert_offer($offer);

                if ($ok === -1) {
                    $error = "You canceled your offer for this item, so you can't bid again.";
                }
                else if ($ok) {
                    $success = "Your bid is accepted.";
                }
                else {
                    $error = "Your bid is not accepted.";
                }
            }
            else {
                $error = "Bid is not valid. Please enter a number.";
            }
        }

        if (isset($_GET["cancelOffer"])) {

        }
    ?>

    <?php if ($success) { ?>
        <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <?php if ($error) { ?>
        <div class="error"><?php echo $error; ?></div>
    <?php } ?>
</center>

<?php include("footer.php"); ?>