-- phpMyAdmin SQL Dump
-- version 4.6.6deb4
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Feb 02, 2020 at 05:40 PM
-- Server version: 10.3.18-MariaDB-0+deb10u1
-- PHP Version: 7.3.11-1~deb10u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `Auction`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`vladimir`@`localhost` PROCEDURE `insert_offer` (IN `amount` DECIMAL, IN `user` VARCHAR(45), IN `item_id` INT, OUT `success` BOOLEAN)  begin
	declare p_item_added DATETIME;
	declare p_item_expiring DATETIME;
	declare p_datetime_now DATETIME;
	declare p_start_price DECIMAL;
	declare p_best_offer DECIMAL;

	select Item.dateAdded, Item.dateExpired into p_item_added, p_item_expiring from Item where Item.itemId = item_id;
	set p_datetime_now = NOW();
	select Item.startPrice into p_start_price from Item where Item.itemId = item_id;
	select max(Offer.amount) into p_best_offer from Offer where Offer.item = item_id and not Offer.canceled;

	if p_datetime_now > p_item_added and p_datetime_now < p_item_expiring and amount > p_start_price and (p_best_offer is null or amount > p_best_offer)
	then
		insert into Offer (amount, time, user, item) values (amount, p_datetime_now, user, item_id);
		set success = true;
	else
		 set success = false;
	end if;

	select success;
end$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Table structure for table `Category`
--

CREATE TABLE `Category` (
  `categoryId` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(245) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Category`
--

INSERT INTO `Category` (`categoryId`, `name`, `description`) VALUES
(1, 'Test category', 'Category for database testing purposes');

-- --------------------------------------------------------

--
-- Table structure for table `DeliveryType`
--

CREATE TABLE `DeliveryType` (
  `deliveryTypeId` int(11) NOT NULL,
  `description` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `DeliveryType`
--

INSERT INTO `DeliveryType` (`deliveryTypeId`, `description`) VALUES
(1, 'Test delivery type');

-- --------------------------------------------------------

--
-- Table structure for table `Item`
--

CREATE TABLE `Item` (
  `itemId` int(11) NOT NULL,
  `name` varchar(45) NOT NULL,
  `description` varchar(245) NOT NULL,
  `dateAdded` datetime NOT NULL,
  `dateExpired` datetime NOT NULL,
  `startPrice` decimal(10,0) NOT NULL,
  `photo` varchar(255) DEFAULT NULL,
  `seller` varchar(45) NOT NULL,
  `deliveryType` int(11) NOT NULL,
  `paymentType` int(11) NOT NULL,
  `category` int(11) NOT NULL,
  `canceled` tinyint(1) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Item`
--

INSERT INTO `Item` (`itemId`, `name`, `description`, `dateAdded`, `dateExpired`, `startPrice`, `photo`, `seller`, `deliveryType`, `paymentType`, `category`, `canceled`) VALUES
(1, 'Test item', 'Item for db testing', '2020-01-13 13:23:53', '2020-01-23 13:24:16', '100', NULL, 'test@user.com', 1, 1, 1, 0),
(2, 'Item', 'Best item ever', '2020-01-31 13:50:12', '2020-02-10 13:50:12', '199', NULL, 'test@user.com', 1, 1, 1, 0),
(3, 'The Item', 'Its just The Item', '2020-02-01 19:11:33', '2020-02-11 19:11:33', '44', '', 'dovla@test.com', 1, 1, 1, 0),
(7, 'dsad', 'dwqdqw q d wqdqw qw ', '2020-02-01 19:31:51', '2020-02-11 19:31:51', '44', '', 'laza@test.com', 1, 1, 1, 1),
(8, 'Item', 'Not sure if it is just an item.', '2020-02-01 19:32:09', '2020-02-11 19:32:09', '99', '', 'laza@test.com', 1, 1, 1, 0),
(9, 'dsadsa', 'dsadasdasdasd', '2020-02-01 19:33:32', '2020-02-11 19:33:32', '111', '', 'laza@test.com', 1, 1, 1, 0),
(10, 'dsadsa', 'dsadasdasdasd', '2020-02-01 19:34:11', '2020-02-11 19:34:11', '111', '', 'laza@test.com', 1, 1, 1, 1),
(11, 'dsadsa', 'dsadasdasdasd', '2020-02-01 19:35:09', '2020-02-11 19:35:09', '111', '../images/', 'laza@test.com', 1, 1, 1, 1),
(12, 'dsadsa', 'dsadasdasdasd', '2020-02-01 19:35:52', '2020-02-11 19:35:52', '111', '../images/1669448_1.jpg', 'laza@test.com', 1, 1, 1, 0),
(13, 'dsadas', 'dsadasdasdasdas', '2020-02-01 19:45:29', '2020-02-11 19:45:29', '45', '../images/1669448_1.jpg', 'dovla@test.com', 1, 1, 1, 1),
(14, 'dsadsa', 'dasdasdas', '2020-02-01 19:47:34', '2020-02-11 19:47:34', '222', '../images/1669448_1.jpg', 'dovla@test.com', 1, 1, 1, 0),
(15, 'dsadsa', 'dasdasdas', '2020-02-01 19:51:12', '2020-02-11 19:51:12', '222', '../images/1669448_1.jpg', 'dovla@test.com', 1, 1, 1, 0),
(16, 'dsadsa', 'dasdasdas', '2020-02-01 19:53:33', '2020-02-11 19:53:33', '222', '../images/1669448_1.jpg', 'dovla@test.com', 1, 1, 1, 0),
(17, 'dsadsa', 'dasdasdas', '2020-02-01 19:54:29', '2020-02-11 19:54:29', '222', '../images/1669448_1.jpg', 'dovla@test.com', 1, 1, 1, 0),
(18, 'cola', 'best of the best', '2020-02-01 19:59:22', '2020-02-11 19:59:22', '49', '../images/1669448_1.jpg', 'dovla@test.com', 1, 1, 1, 0),
(19, 'watsit', '', '2020-01-12 21:38:42', '2020-01-22 21:38:52', '199', '../images/154602194146176016.png', 'dovla@test.com', 1, 1, 1, 0),
(20, 'canceled', '', '2020-02-01 21:13:11', '2020-02-11 21:13:11', '199', '', 'dovla@test.com', 1, 1, 1, 1),
(21, 'new item', '', '2020-02-01 21:13:46', '2020-02-11 21:13:46', '199', '', 'dovla@test.com', 1, 1, 1, 1),
(22, 'Square item', 'You must have this item.', '2020-02-01 21:52:17', '2020-02-11 21:52:17', '499', '../images/programmer.jpg', 'dovla@test.com', 1, 1, 1, 0),
(23, 'just item', 'yes its just item', '2020-01-18 17:19:15', '2020-01-28 17:19:29', '99', '', 'test@example.com', 1, 1, 1, 0),
(24, 'test auction', '', '2020-02-02 17:33:31', '2020-02-12 17:33:31', '299', '', 'test@test.com', 1, 1, 1, 1),
(25, 'test', 'one more for testing', '2020-02-02 17:33:59', '2020-02-12 17:33:59', '399', '', 'test@test.com', 1, 1, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Offer`
--

CREATE TABLE `Offer` (
  `offerId` int(11) NOT NULL,
  `amount` decimal(10,0) NOT NULL,
  `time` datetime NOT NULL,
  `canceled` tinyint(1) NOT NULL DEFAULT 0,
  `user` varchar(45) NOT NULL,
  `item` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `Offer`
--

INSERT INTO `Offer` (`offerId`, `amount`, `time`, `canceled`, `user`, `item`) VALUES
(1, '120', '2020-01-28 13:11:56', 0, 'test@user.com', 1),
(2, '200', '2020-01-31 13:56:18', 1, 'laza@test.com', 2),
(3, '202', '2020-01-31 16:37:03', 0, 'test@user.com', 2),
(4, '203', '2020-01-31 18:04:38', 1, 'laza@test.com', 2),
(5, '204', '2020-01-31 18:15:05', 1, 'laza@test.com', 2),
(6, '205', '2020-01-31 18:20:07', 1, 'laza@test.com', 2),
(7, '207', '2020-01-31 18:27:51', 1, 'laza@test.com', 2),
(8, '203', '2020-02-01 15:19:07', 1, 'laza@test.com', 2),
(9, '203', '2020-02-01 15:19:23', 1, 'laza@test.com', 2),
(10, '204', '2020-02-01 16:41:41', 1, 'laza@test.com', 2),
(11, '210', '2020-02-01 16:44:38', 1, 'laza@test.com', 2),
(12, '211', '2020-02-01 16:45:03', 1, 'laza@test.com', 2),
(13, '212', '2020-02-01 16:45:23', 1, 'laza@test.com', 2),
(14, '213', '2020-02-01 16:45:39', 1, 'laza@test.com', 2),
(15, '203', '2020-02-01 16:50:49', 1, 'dovla@test.com', 2),
(16, '204', '2020-02-01 16:51:11', 1, 'dovla@test.com', 2),
(17, '205', '2020-02-01 16:52:47', 1, 'dovla@test.com', 2),
(18, '206', '2020-02-01 17:03:40', 1, 'test@test.com', 2),
(19, '210', '2020-02-01 17:04:43', 0, 'user@example.com', 2),
(20, '211', '2020-02-01 17:05:07', 1, 'dovla@test.com', 2),
(21, '222', '2020-02-01 20:06:43', 0, 'laza@test.com', 19),
(22, '224', '2020-02-01 21:29:22', 0, 'test@example.com', 19),
(23, '555', '2020-02-01 21:52:49', 0, 'test@example.com', 22),
(24, '600', '2020-02-01 21:53:07', 0, 'laza@test.com', 22),
(25, '111', '2020-02-02 17:18:05', 0, 'dovla@test.com', 23);

-- --------------------------------------------------------

--
-- Table structure for table `PaymentType`
--

CREATE TABLE `PaymentType` (
  `paymentTypeId` int(11) NOT NULL,
  `description` varchar(45) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `PaymentType`
--

INSERT INTO `PaymentType` (`paymentTypeId`, `description`) VALUES
(1, 'Test payment type');

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE `User` (
  `email` varchar(45) NOT NULL,
  `firstName` varchar(45) NOT NULL,
  `lastName` varchar(45) NOT NULL,
  `password` varchar(245) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`email`, `firstName`, `lastName`, `password`) VALUES
('alek@test.com', 'aleksandar', 'jankovic', '$2y$10$WMY3Cfiu0BrCuBPIhoN5quaaPy8CfR.2zD2a1lMAhSjn9oXmSxD26'),
('cosma@dovla.test', 'dovla', 'cosma', '$2y$10$xPcOuh/zeNlZCeJgoNyYOeC8VkV/ZzGLkXeW3WUHc9qXibHotwA1.'),
('cosma@test.com', 'dovla', 'cosma', '$2y$10$fOXpRy.UzO8nenW7FgUL4.wCnDJPx9mV3N2xelpvaXEyj8gim1XGm'),
('dovla@test.com', 'dovla', 'cosma', '$2y$10$fcUW41CWRlgCJc9UjJ6bAe3CPY2x0Eb9eLDSjiKv6S2Plr/wE5qGO'),
('laza@test.com', 'lazar', 'trajkovic', '$2y$10$LCAwu9r8dQdH575a8EwPou9RF0Rf.3Dsa78ecfo04nVwa7ufzY5Ri'),
('test@example.com', 'testname', 'testsurname', '$2y$10$9/48NCVodSfwKgORx2V8dOADYFlt1sty1ev/SotOx3nk68nwWf2lC'),
('test@test.com', 'test', 'test', '$2y$10$/8emxOeSlWdLoAA5gUUvHeG.4MB7KzyH0k.ZcUrcuLUerCCZIO0Xu'),
('test@user.com', 'Testname', 'Testsurname', 'testpass'),
('user@example.com', 'user', 'user', '$2y$10$JtRRTQuGchtyTlwviw3ZcuMnPC7a1/DW657z438lLYb.voyfRITXe');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Category`
--
ALTER TABLE `Category`
  ADD PRIMARY KEY (`categoryId`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `DeliveryType`
--
ALTER TABLE `DeliveryType`
  ADD PRIMARY KEY (`deliveryTypeId`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indexes for table `Item`
--
ALTER TABLE `Item`
  ADD PRIMARY KEY (`itemId`),
  ADD KEY `Item_fk0` (`seller`),
  ADD KEY `Item_fk1` (`deliveryType`),
  ADD KEY `Item_fk2` (`paymentType`),
  ADD KEY `Item_fk3` (`category`);

--
-- Indexes for table `Offer`
--
ALTER TABLE `Offer`
  ADD PRIMARY KEY (`offerId`),
  ADD KEY `Offer_fk0` (`user`),
  ADD KEY `Offer_fk1` (`item`);

--
-- Indexes for table `PaymentType`
--
ALTER TABLE `PaymentType`
  ADD PRIMARY KEY (`paymentTypeId`),
  ADD UNIQUE KEY `description` (`description`);

--
-- Indexes for table `User`
--
ALTER TABLE `User`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Category`
--
ALTER TABLE `Category`
  MODIFY `categoryId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `DeliveryType`
--
ALTER TABLE `DeliveryType`
  MODIFY `deliveryTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `Item`
--
ALTER TABLE `Item`
  MODIFY `itemId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `Offer`
--
ALTER TABLE `Offer`
  MODIFY `offerId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;
--
-- AUTO_INCREMENT for table `PaymentType`
--
ALTER TABLE `PaymentType`
  MODIFY `paymentTypeId` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `Item`
--
ALTER TABLE `Item`
  ADD CONSTRAINT `Item_fk0` FOREIGN KEY (`seller`) REFERENCES `User` (`email`),
  ADD CONSTRAINT `Item_fk1` FOREIGN KEY (`deliveryType`) REFERENCES `DeliveryType` (`deliveryTypeId`),
  ADD CONSTRAINT `Item_fk2` FOREIGN KEY (`paymentType`) REFERENCES `PaymentType` (`paymentTypeId`),
  ADD CONSTRAINT `Item_fk3` FOREIGN KEY (`category`) REFERENCES `Category` (`categoryId`);

--
-- Constraints for table `Offer`
--
ALTER TABLE `Offer`
  ADD CONSTRAINT `Offer_fk0` FOREIGN KEY (`user`) REFERENCES `User` (`email`),
  ADD CONSTRAINT `Offer_fk1` FOREIGN KEY (`item`) REFERENCES `Item` (`itemId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
